var squareDivs = document.getElementById("container").querySelectorAll(".colorSquare");
var squares = [];
var targetColorDisplay = document.getElementById("targetColor");
var targetColor;
var h1 = document.querySelector("h1");
var message = document.getElementById('message');
var modeButtons = document.querySelectorAll(".mode");
var resetButton = document.getElementById("resetButton");
var numSquares = 6;

setUp();

function selectEasyButton() {
	easyButton.classList.add("selected");
};

function addListenersToModeButtons() {
	
	for (var i = modeButtons.length - 1; i >= 0; i--) {
	 modeButtons[i].addEventListener("click", function() {
	 	for (var i = modeButtons.length - 1; i >= 0; i--) {
	 		modeButtons[i].classList.remove("selected");
	 	}
	 	this.classList.add("selected");
	 	this.textContent === "Easy" ? numSquares = 6: numSquares = 3;
	 		resetGame(numSquares);
	 })
	}
}

function addListenerToResetButton() {
	resetButton.addEventListener("click", function() {
		resetGame(numSquares);	
});	
}

function resetGame(num) {
	resetButton.textContent = "New Colors";
	colorizeSquares(num);
	setTargetColor();
	h1.style.background = "steelblue";
	message.textContent = "";
}

/*generate a random numbers for r,g, and b values*/
function randomNumber(max) { 
	var random = Math.floor(Math.random() * max);
	return random;
}

/*generate a random color*/
function randomColor() {
	var r = randomNumber(256).toString();
	var g = randomNumber(256).toString();
	var b = randomNumber(256).toString();

	var color = "rgb(" + r + "," + g + "," + b + ")";
	return color; 
}

function colorizeSquares(num) {

	/*color the rest of the divSquares to the background color*/
	for (var i = 0; i < squareDivs.length; i++) {{
		squareDivs[i].style.backgroundColor = "#232323";
	}}

	/*reset the squares array*/
	squares = [];
	for (var i = 0; i < num; i++) {
		squares.push(squareDivs[i]);
		/*colorize the squares randomly*/
		squares[i].style.backgroundColor = randomColor();
		console.log(squares[i].style.backgroundColor)
	}
}
	
function setTargetColor() {
	targetColor = squares[randomNumber(squares.length)].style.backgroundColor;
	console.log("TARGET:" + targetColor);
	targetColorDisplay.textContent = targetColor;
}

/*add event listeners to the squares*/
function addListenersToSquares() {
	for (var i = squares.length - 1; i >= 0; i--) {
	
		squares[i].addEventListener("click", function() {
			
			if (this.style.backgroundColor == targetColor) {

				/*change the color of header and all other squares into the target color*/
				h1.style.background = targetColor;
				squares.forEach(function(element) {
					element.style.backgroundColor = targetColor;
				});
				message.textContent = "Correct";
				resetButton.textContent = "Play Again?";

			} else {
				this.style.backgroundColor = "#232323"
				message.textContent = "Try again";
			}
		});
	}
}

function setUp() {
	colorizeSquares(numSquares);
	setTargetColor();
	addListenersToModeButtons();
	addListenerToResetButton();
	addListenersToSquares();
	selectEasyButton();	
}

